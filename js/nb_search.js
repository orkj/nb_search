(function ($) {	
  $(document).ready(function () {
    $('.nb-search-block').each(function(i,n) {
      $(this).prepend('<span class="nb-sortby-a"></span><span class="nb-sortby-1"></span>');
    });
    $('.nb-sortby-a').click(function() {
      var list = $(this).parent().find('li');
      $(list).tsort();
    });
    $('.nb-sortby-1').click(function() {
      var list = $(this).parent().find('li');
      $(list).tsort('.nb-count', {order: 'desc'});
    });
    $('.digitalcontainer').hide();
    $('.nb-expand-dig').show().click(function() {
      $(this).parent().find('.digitalcontainer').toggle();
    })
  });
})(jQuery);
