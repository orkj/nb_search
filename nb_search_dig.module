<?php

/**
 * @file
 * Search functions for search in Nasjonalbiblioteket library search.
 */

/**
 * Implements hook_menu().
 */
function nb_search_dig_menu() {
  $items['admin/config/search/nb_search_dig'] = array(
    'title' => 'Nasjonalbiblioteket digital search settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nb_search_dig_settings'),
    'description' => 'Settings for the NB search module',
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/nb_search_dig.pages.inc',
  );
  $items['nb/digsearch/%nb_search_dig_list'] = array(
    'title' => 'Search results',
    'page callback' => 'nb_search_dig_list',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'file' => 'includes/nb_search_dig.pages.inc',
  );
  $items['nb/digsearch/%nb_search_dig_list/view'] = array(
    'title' => variable_get('nb_search_dig_title', 'NB Digital Search'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['nb/digsearch/%nb_search_dig_list/redirect'] = array(
    'title' => variable_get('nb_search_bib_title', 'NB Library Search'),
    'page callback' => 'nb_search_dig_redir',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['nb/digsearch/object/%nb_search_dig_mods'] = array(
    'title' => 'Search results',
    'page callback' => 'nb_search_dig_mods',
    'page arguments' => array(3),
    'access arguments' => array('access content'),
    'file' => 'includes/nb_search_dig.pages.inc',
  );
  $items['nb/digsearch'] = array(
    'title' => variable_get('nb_search_dig_title', 'NB Digital Search'),
    'page callback' => 'nb_search_dig_search_page',
    'access arguments' => array('access content'),
  );
  return $items;
}

/**
 * Implements hook_block_info().
 */
function nb_search_dig_block_info() {
  $block = array();
  $facets = nb_search_dig_facet_info();
  foreach ($facets as $facet) {
    $block[$facet] = array(
      'info' => t('NB Digital search facet: !facetname', array('!facetname' => $facet)),
      'cache' => DRUPAL_NO_CACHE,
    );
  }
  return $block;
}

/**
 * Implements hook_block_view().
 */
function nb_search_dig_block_view($delta = '') {
  $block = array();
  $facets = nb_search_dig_facet_info();
  $list = array();
  $params = nb_search_api_get_params('nb_search_dig_list');
  if (empty($params)) {
    return;
  }
  $query = '';
  foreach ($params['queryitems'] as $key => $value) {
    $key = urlencode($key);
    $value = urlencode($value);
    $query .= $key . '=' . $value . '&';
  }
  drupal_add_js(drupal_get_path('module', 'nb_search_api') . '/js/nb_search.js');
  drupal_add_js(drupal_get_path('module', 'nb_search_api') . '/js/jquery.tinysort.js');
  foreach ($facets as $facet) {
    switch ($delta) {
      case $facet:
        $facetarray = nb_search_api_make_facet($facet, 'nb_search_dig_list');
        if (empty($facetarray)) {
          break;
        }
        foreach ($facetarray as $key => $value) {
          $append = $query . 'fq=' . urlencode($facet . ':"' . $key . '"');
          if ($facet == 'year') {
            $append = $query . 'fq=' . urlencode($facet . ':' . $key . '');
          }
          global $base_url;
          $url = url($base_url . '/nb/digsearch/search?' . $append);
          $list[] = l($key, $url) . ' (<span class="nb-count">' . $value . '</span>) ' . t('hits');
        }
        $content = array(
          '#theme' => 'item_list',
          '#items' => $list,
          '#attributes' => array('class' => 'nb-search-block'),
        );
        $block['subject'] = t('Filter by !facetname', array('!facetname' => $facet));
        $block['content'] = $content;
        break;
    }
  }
  return $block;
}

/**
 * Function to return an array of all available facets.
 *
 * Would not want to write that multiple times :)
 *
 * @return array
 *   An array containing all available facets.
 */
function nb_search_dig_facet_info() {
  return array(
    'mediatype',
    'year',
    'languages',
  );
}

function nb_search_dig_redir($data) {
  return '';
}

/**
 * Render a search form page.
 */
function nb_search_dig_search_page() {
  return drupal_get_form('nb_search_dig_search_form');
}

/**
 * Loads a search list object.
 *
 * @param str $query
 *   Not used.
 *
 * @return object
 *   A drupal_http_request object.
 */
function nb_search_dig_list_load($query) {
  if (arg(3) == 'redirect') {
    $ref = parse_url($_SERVER['HTTP_REFERER']);
    global $base_url;
    $url = url($base_url . '/nb/bibsearch/search?' . $ref['query']);
    drupal_goto($url);
  }
  $params = $_GET;
  $fqs = '';
  unset($params['q']);
  unset($params['query']);
  foreach ($params as $key => $value) {
    $fqs .= '&' . urlencode($key) . '=' . urlencode($value);
  }
  $query = urlencode($_GET['query']);
  $rows = 10;
  $page = 0;
  if (!empty($_GET['page'])) {
    $page = check_plain($_GET['page']);
  }
  $xml = nb_search_api_search($query, 'dig', $fqs, $rows, $page);
  return $xml;
}

/**
 * Loads a mods item object.
 *
 * @param str $id
 *   An ID representing the object in the XML.
 *
 * @return object
 *   A drupal_http_request object.
 */
function nb_search_dig_mods_load($id) {
  $xml = nb_search_api_bib_mods($id, 'dig');
  return $xml;
}

/**
 * Loads a holdings item object.
 *
 * @param str $id
 *   An ID representing the object in the XML.
 *
 * @return object
 *   A drupal_http_request object.
 */
function nb_search_dig_holdings_load($id) {
  $xml = nb_search_api_bib_holdings($id);
  return $xml;
}

/**
 * Implements hook_form().
 */
function nb_search_dig_search_form($form, &$form_state) {
  $params = nb_search_api_get_params('nb_search_dig_list');
  $query = '';
  if (!empty($params)) {
    $query = $params['query'];
  }
  $form['query'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter a search term'),
    '#size' => 60,
    '#maxlength' => 256,
    '#default_value' => $query,
  );
  $form['submit_query'] = array(
    '#type' => 'submit',
    '#value' => t('Find'),
  );
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced search settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['advanced']['mediatype'] = array(
    '#type' => 'select',
    '#title' => t('Media type'),
    '#options' => array(
      '' => t('- Select -'),
      urlencode('Aviser') => t('Newspapers'),
      urlencode('B�ker') => t('Books'),
      urlencode('Nettsider') => t('Websites'),
      urlencode('Musikk') => t('Music'),
      urlencode('Noter') => t('Musical scores'),
      urlencode('Artikler') => t('Aricles'),
      urlencode('Ukjent') => t('Unknown'),
      urlencode('Tidsskrift') => t('Magazines'),
      urlencode('Kart') => t('Maps'),
      urlencode('Film') => t('Movies'),
      urlencode('Radio') => t('Radio'),
      urlencode('Lydopptak') => t('Sound recording'),
      urlencode('Bilder') => t('Pictures'),
      urlencode('Plakater') => t('Posters'),
      urlencode('Privatarkivmateriale') => t('Material from private archives'),
      urlencode('Musikkmanuskripter') => t('Musical manuscripts'),
      urlencode('Gjenstander') => t('Objects'),
    ),
  );
  $form['advanced']['submit_query2'] = array(
    '#type' => 'submit',
    '#value' => t('Find'),
  );
  return $form;
}


/**
 * Implements hook_form_submit().
 */
function nb_search_dig_search_form_submit($form, &$form_state) {
  $query = $form_state['values']['query'];
  if (empty($query)) {
    $query = 'metadataclasses:"public"';
  }
  $mediatype = $form_state['values']['mediatype'];
  global $base_url;
  $redirect_url = $base_url . '/nb/digsearch/search?query=' . $query;
  $mediatype = $form_state['values']['mediatype'];
  if (!empty($mediatype)) {
    $redirect_url = $base_url . '/nb/digsearch/search?query=' . $query . '&fq=mediatype:"' . $mediatype . '"';
  }
  $form_state['redirect'] = $redirect_url;
}
