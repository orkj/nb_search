<?php

/**
 * @file
 * Generates the search result list for the NB search.
 *
 * Contact: eirik@nymedia.no
 */

/**
 * Returns the settings page for the module.
 */
function nb_search_bib_settings($form, &$form_state) {
  $form['page-settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page display settings.'),
  );
  $form['page-settings']['nb_search_bib_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for the NB Library search page.'),
    '#description' => t('Please enter a title to use on all pages associated with NB Library search.'),
    '#default_value' => variable_get('nb_search_bib_title', 'NB Library Search'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Function to create the list view page.
 *
 * @param object $xml
 *   A drupal_http_request object.
 *
 * @return mixed
 *   The search result list page.
 */
function nb_search_bib_list($xml) {
  $return = '';
  if ($xml->code !== '200') {
    // @todo set message and return!!!!
    drupal_set_message(t('There was an error while doing a search. The server responded with the code @code, and the message %message', array('@code' => $xml->code, '%message' => $xml->error)), 'warning', FALSE);
    return '';
  }
  drupal_add_css(drupal_get_path('module', 'nb_search_api') . '/css/stylesheets/screen.css');
  $data = simplexml_load_string($xml->data);
  $ns_ns2 = $data->children('http://a9.com/-/spec/opensearch/1.1/');
  $total_records = $ns_ns2->totalResults;
  
  $params = nb_search_api_get_params('nb_search_bib_list');
  if (empty($params)) {
    // Hmm, is that possible?
  }
  $querystring = '';
  foreach ($params['queryitems'] as $key => $value) {
    $key = urlencode($key);
    $value = urlencode($value);
    $querystring .= $key . '=' . $value . '&';
  }
  $querystring = substr($querystring, 0, -1);
  global $base_url;
  $url = url($base_url . '/nb/digsearch/search?' . $querystring);
  $list = array(
    'items' => array(
      array(
        'data' => l(t('NB Digital search'), $url),
      ),
      array(
        'data' => l(t('NB Library search'), '#'),
        'class' => array(
          'active',
        ),
      )
    ),
    'title' => NULL,
    'type' => 'ul',
    'attributes' => array('class' => 'tabs primary'),
  );
  $render = array(
    '#theme' => 'item-list',
    '#items' => $list,
  );
  //$return .= '<div class="tabs">' . theme_item_list($list) . '</div>';
  $variables = array(
    'primary' => array(
      array(
        '#theme' => 'menu_local_task',
        '#link' => array('test', 'test')
      ),
      array(
        '#theme' => 'menu_local_task',
        '#link' => array('test', 'test')
      ),
    ),
  );
  //$return .= theme_menu_local_tasks($variables);
  $query = implode('', $data->xpath('//ns2:Query/@searchTerms'));
  $form = drupal_get_form('nb_search_bib_search_form');
  $return .= render($form);
  $return .= '<div class="nb-results">';
  $return .= '<div class="nb-total-records">' . t('Search returned !num hits', array('!num' => $total_records)) . '</div>';
  $return .= nb_search_api_make_item_list($data->entry);
  $num_per_page = variable_get('nb_search_api_num_rows', 10);
  $pager = pager_default_initialize($total_records, $num_per_page);
  $variables = array(
    'parameters' => array(
      'query' => $query,
      'rows' => $num_per_page,
    ),
  );
  $return .= theme('pager', $variables);
  $return .= '</div>';
  return $return;
}

/**
 * Function to create the object view page.
 *
 * @param object $xml
 *   A drupal_http_request object.
 *
 * @return mixed
 *   The search result list page.
 */
function nb_search_bib_mods($xml) {
  if ($xml->code != '200') {
    drupal_set_message(t('There was an error while looking up this object. The server responded with the code @code, and the message %message', array('@code' => $xml->code, '%message' => $xml->error)), 'warning', FALSE);
    return '';
  }
  $data = simplexml_load_string($xml->data);
  $data->registerXPathNamespace("mods", "http://www.loc.gov/mods/v3");
  drupal_set_title($data->titleInfo->title);
  $return = '';
  $return .= '<dl>';
  foreach ($data as $item => $value) {
    switch ($item) {
      case 'language':
        $return .= '<dt>' . t('Language') . '</dt>';
        $return .= '<dd>' . $data->$item->languageTerm . '</dd>';
        continue;
      case 'physicalDescription':
        $return .= '<dt>' . t('Form') . '</dt>';
        $return .= '<dd>' . $data->$item->form . '</dd>';
        $return .= '<dt>' . t('Extent') . '</dt>';
        $return .= '<dd>' . $data->$item->extent . '</dd>';
        continue;
      case 'subject':
        if (!empty($data->$item->topic)) {
          $return .= '<dt>' . t('Subject') . '</dt>';
          $return .= '<dd>' . $data->$item->topic . '</dd>';
        }
        continue;
      case 'name':
        $return .= '<dt>' . t('Name') . '</dt>';
        $return .= '<dd>' . implode('', $data->xpath('mods:name/mods:namePart[text()="' . $value->namePart[0] . '"]')) . '</dd>';
        continue;
      case 'titleInfo':
        continue;
      case 'location':
        continue;
      case 'typeOfResource':
        $return .= '<dt>' . t('Type of resource') . '</dt>';
        $return .= '<dd>' . $value . '</dd>';
        continue;
      case 'recordInfo':
        $cdate = trim($data->$item->recordCreationDate);
        if (!empty($cdate)) {
          $return .= '<dt>' . t('Record creation date') . '</dt>';
          $return .= '<dd>' . $data->$item->recordCreationDate . '</dd>';
        }
        $return .= '<dt>' . t('Record changed date') . '</dt>';
        $return .= '<dd>' . $data->$item->recordChangeDate . '</dd>';
        $return .= '<dt>' . t('Record indentifier') . '</dt>';
        $return .= '<dd>' . $data->$item->recordIdentifier . '</dd>';
        continue;
      case 'originInfo':
        $return .= '<dt>' . t('Date issued') . '</dt>';
        $return .= '<dd>' . $data->$item->dateIssued . '</dd>';
        $return .= '<dt>' . t('Issuance') . '</dt>';
        $return .= '<dd>' . $data->$item->issuance . '</dd>';
        continue;
      case 'identifier':
        $return .= '<dt>' . $item . ' (' . implode('', $data->xpath('//mods:identifier[text()="' . $value . '"]/@type')) . ')</dt>';
        $return .= '<dd>' . $value . '</dd>';
        continue;
      case 'classification':
        $return .= '<dt>' . $item . ' (' . implode('', $data->xpath('//mods:classification[text()="' . $value . '"]/@authority')) . ')</dt>';
        $return .= '<dd>' . $value . '</dd>';
        continue;
      default:
        $return .= '<dt>' . $item . '</dt>';
        $return .= '<dd>' . $value . '</dd>';
        continue;
        // /switch
    }
  }
  $id = arg(3);
  $digital = nb_search_api_get_digital_object($id);
  if ($digital !== 0) {
    $return .= l(t('Show the digital object of !title', array('!title' => check_plain($data->titleInfo->title))), $digital);
  }
  $persons = $data->xpath('//mods:subject/mods:name');
  if (!empty($persons)) {
    $return .= '<dt>' . t('About person') . '</dt>';
    foreach ($data->xpath('//mods:subject') as $name) {
      $return .= '<dd>' . $name->name->namePart . '</dd>';
    }
  }

  $return .= '</dl>';
  return $return;
}

/**
 * Function to create the holdings page.
 *
 * @param object $data
 *   A drupal_http_request object.
 *
 * @return mixed
 *   The search result list page.
 */
function nb_search_bib_holdings($data) {
  $xml = simplexml_load_string($data->data);
  $bibnr = $xml->xpath('//bibnr');
  $return = '<div class="holdings-wrapper">';
  $return .= t('The follwing libraries have this book in their collection.');
  $i = 0;
  foreach ($bibnr as $bib) {
    $id = substr($bib, 3);
    if (empty($id)) {
      // No ID? Let's skip this one.
      continue;
    }
    $library_info = nb_search_api_get_library_meta($id);
    if ($library_info->code != 200) {
      // HTTP status not OK? Let's not process this.
      continue;
    }
    $library_xml = simplexml_load_string($library_info->data);
    // Add class for zebra striping.
    $rowclass = ' odd';
    if ($i % 2 > 0) {
      $rowclass = ' even';
    }
    if (empty($library_xml->record)) {
      // Not a valid library ID. skip it.
      continue;
    }
    $return .= '<div class="library-single' . $rowclass . '">';
    $return .= '<h3>' . strip_tags($library_xml->record->inst->asXML()) . '</h3>';
    $return .= '<div class="bib-address">' . t('Address') . ': ' . strip_tags($library_xml->record->vadr->asXML()) . '</div>';
    $return .= '<div class="bib-postcode">' . strip_tags($library_xml->record->vpostnr->asXML()) . ' ' . strip_tags($library_xml->record->vpoststed->asXML()) . '</div>';
    $return .= '<div class="telefon">' . t('Telephone number') . ': ' . strip_tags($library_xml->record->tlf->asXML()) . '</div>';
    $return .= '</div>';
    $id = substr($bib, 3);
    $i++;
  }
  $return .= '</div>';
  return $return;
}
