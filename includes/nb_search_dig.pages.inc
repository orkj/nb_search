<?php

/**
 * @file
 * Generates the search result list for the NB search.
 *
 * Contact: eirik@nymedia.no
 */

/**
 * Returns the settings page for the module.
 */
function nb_search_dig_settings($form, &$form_state) {
  $form['page-settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page display settings.'),
  );
  $form['page-settings']['nb_search_dig_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for the NB Digital search page.'),
    '#description' => t('Please enter a title to use on all pages associated with NB Digital search.'),
    '#default_value' => variable_get('nb_search_dig_title', 'NB Digital Search'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Function to create the list view page.
 *
 * @param object $xml
 *   A drupal_http_request object.
 *
 * @return mixed
 *   The search result list page.
 */
function nb_search_dig_list($xml) {
  $return = '';
  if ($xml->code !== '200') {
    drupal_set_message(t('There was an error while doing a search. The server responded with the code @code, and the message %message', array('@code' => $xml->code, '%message' => $xml->error)), 'warning', FALSE);
    return '';
  }
  $params = nb_search_api_get_params('nb_search_dig_list');
  if (empty($params)) {
    // Hmm, is that possible?
  }
  $querystring = '';
  foreach ($params['queryitems'] as $key => $value) {
    $key = urlencode($key);
    $value = urlencode($value);
    $querystring .= $key . '=' . $value . '&';
  }
  $querystring = substr($querystring, 0, -1);
  global $base_url;
  $data = simplexml_load_string($xml->data);
  $ns_ns2 = $data->children('http://a9.com/-/spec/opensearch/1.1/');
  $total_records = $ns_ns2->totalResults;
  $query = implode('', $data->xpath('//ns2:Query/@searchTerms'));
  $form = drupal_get_form('nb_search_dig_search_form');
  $return .= render($form);
  $return .= '<div class="nb-results">';
  $return .= '<div class="nb-total-records">' . t('Search returned !num hits', array('!num' => $total_records)) . '</div>';
  $return .= nb_search_api_make_item_list($data->entry);
  $num_per_page = variable_get('nb_search_api_num_rows', 10);
  $pager = pager_default_initialize($total_records, $num_per_page);
  $variables = array(
    'parameters' => array(
      'query' => $query,
      'rows' => $num_per_page,
    ),
  );
  $return .= theme('pager', $variables);
  $return .= '</div>';
  return $return;
}

/**
 * Function to create the object view page.
 *
 * @param object $xml
 *   A drupal_http_request object.
 *
 * @return mixed
 *   The search result list page.
 */
function nb_search_dig_mods($xml) {
  if ($xml->code !== '200') {
    drupal_set_message(t('There was an error while looking up this object. The server responded with the code @code, and the message %message', array('@code' => $xml->code, '%message' => $xml->error)), 'warning', FALSE);
    return '';
  }
  $data = simplexml_load_string($xml->data);
  $data->registerXPathNamespace("mods", "http://www.loc.gov/mods/v3");
  $return = '';
  $return .= '<dl>';
  foreach ($data->record as $item) {
    $ns_mods = $item->children('http://www.loc.gov/mods/v3');
    $mods = $ns_mods->mods;
    $title = $mods[0]->titleInfo->title;
    if (!empty($mods[0]->titleInfo->subTitle)) {
      $title .= ' (' . $mods[0]->titleInfo->subTitle . ')';
    }
    drupal_set_title($title);
    global $base_url;
    foreach ($mods[0] as $item => $value) {
      switch ($value->getName()) {
        case 'subject':
          continue;
        case 'physicalDescription':
          if (!empty($value->extent)) {
            $return .= '<dt>' . t('Extent') . '</dt>';
            $return .= '<dd>' . $value->extent . '</dd>';
          }
          if (!empty($value->form)) {
            $return .= '<dt>' . t('Form') . '</dt>';
            foreach ($value->form as $form) {
              $return .= '<dd>' . $form . '</dd>';
            }
          }
          continue;
        case 'name':
          $return .= '<dt>' . t('Name') . '</dt>';
          $return .= '<dd>' . implode('', $data->xpath('//mods:name/mods:namePart[text()="' . $value->namePart[0] . '"]')) . '</dd>';
          continue;
        case 'titleInfo':
          continue;
        case 'location':
          continue;
        case 'typeOfResource':
          $return .= '<dt>' . t('Type of resource') . '</dt>';
          $return .= '<dd>' . $value . '</dd>';
          continue;
        case 'recordInfo':
          if (!empty($value->recordContentSource)) {
            $return .= '<dt>' . t('Record content source') . '</dt>';
            $return .= '<dd>' . $value->recordContentSource . '</dd>';
          }
          continue;
        case 'relatedItem':
          $type = implode('', $value->xpath('@type'));
          if ($type == 'host' && !empty($value->titleInfo->title)) {
            $return .= '<dt>' . t('Related item') . '</dt>';
            $return .= '<dd>' . $value->titleInfo->title . '</dd>';
          }
          elseif ($type == 'succeeding') {
            $return .= '<dt>' . t('Related item (next issue)') . '</dt>';
            $value->registerXPathNamespace("xlink", "http://www.w3.org/1999/xlink");
            $url = url($base_url . '/nb/digsearch/search?query=urn:"' . implode('', $value->xpath('@xlink:href')) . '"');
            $return .= '<dd>' . l(implode('', $value->xpath('@xlink:title')), $url) . '</dd>';
          }
          elseif ($type == 'preceding') {
            $return .= '<dt>' . t('Related item (previous issue)') . '</dt>';
            $value->registerXPathNamespace("xlink", "http://www.w3.org/1999/xlink");
            $url = url($base_url . '/nb/digsearch/search?query=urn:"' . implode('', $value->xpath('@xlink:href')) . '"');
            $return .= '<dd>' . l(implode('', $value->xpath('@xlink:title')), $url) . '</dd>';
          }
          continue;
        case 'extension':
          continue;
        case 'originInfo':
          if (!empty($value->dateIssued)) {
            $return .= '<dt>' . t('Date issued') . '</dt>';
            $return .= '<dd>' . $value->dateIssued . '</dd>';
          }
          if (!empty($value->place)) {
            $return .= '<dt>' . t('Origin place') . '</dt>';
            $return .= '<dd>' . $value->place->placeTerm . '</dd>';
          }
          continue;
        case 'identifier':
          $return .= '<dt>' . $item . ' (' . implode('', $value->xpath('@type')) . ')</dt>';
          $return .= '<dd>' . $value . '</dd>';
          continue;
        case 'classification':
          $return .= '<dt>' . $item . ' (' . implode('', $value->xpath('@authority')) . ')</dt>';
          $return .= '<dd>' . $value . '</dd>';
          continue;
        case 'language':
          $return .= '<dt>' . t('Language') . '</dt>';
          $return .= '<dd>' . $value->languageTerm . '</dd>';
          continue;
        default:
          $return .= '<dt>' . $item . '</dt>';
          $return .= '<dd>' . $value . '</dd>';
          continue;
          // /switch
      }
    }
  }
  if (!empty($value->topic)) {
    $return .= '<dt>' . t('Subject') . '</dt>';
    $return .= '<dd>' . $value->topic . '</dd>';
  }
  $topics = $data->xpath('//mods:subject/mods:topic');
  if (!empty($topics)) {
    $return .= '<dt>' . t('About subject') . '</dt>';
    foreach ($topics as $topic) {
      $return .= '<dd>' . $topic . '</dd>';
    }
  }
  $persons = $data->xpath('//mods:subject/mods:name');
  if (!empty($persons)) {
    $return .= '<dt>' . t('About person') . '</dt>';
    foreach ($persons as $name) {
      $return .= '<dd>' . implode('', $name->xpath('mods:namePart')) . '</dd>';
    }
  }
  $id = arg(3);
  $digital = nb_search_api_get_digital_object($id);
  if ($digital !== 0) {
    $return .= l(t('Show the digital object of !title', array('!title' => check_plain($title))), $digital);
  }

  $return .= '</dl>';
  return $return;
}

/**
 * Function to create the holdings page.
 *
 * @param object $data
 *   A drupal_http_request object.
 *
 * @return mixed
 *   The search result list page.
 */
function nb_search_dig_holdings($data) {
  $xml = simplexml_load_string($data->data);
  $bibnr = $xml->xpath('//bibnr');
  $return = '<div class="holdings-wrapper">';
  $return .= t('The follwing libraries have this book in their collection.');
  $i = 0;
  foreach ($bibnr as $bib) {
    $id = substr($bib, 3);
    if (empty($id)) {
      // No ID? Let's skip this one.
      continue;
    }
    $library_info = nb_search_api_get_library_meta($id);
    if ($library_info->code != 200) {
      // HTTP status not OK? Let's not process this.
      continue;
    }
    $library_xml = simplexml_load_string($library_info->data);
    // Add class for zebra striping.
    $rowclass = ' odd';
    if ($i % 2 > 0) {
      $rowclass = ' even';
    }
    if (empty($library_xml->record)) {
      // Not a valid library ID. skip it.
      continue;
    }
    $return .= '<div class="library-single' . $rowclass . '">';
    $return .= '<h3>' . strip_tags($library_xml->record->inst->asXML()) . '</h3>';
    $return .= '<div class="bib-address">' . t('Address') . ': ' . strip_tags($library_xml->record->vadr->asXML()) . '</div>';
    $return .= '<div class="bib-postcode">' . strip_tags($library_xml->record->vpostnr->asXML()) . ' ' . strip_tags($library_xml->record->vpoststed->asXML()) . '</div>';
    $return .= '<div class="telefon">' . t('Telephone number') . ': ' . strip_tags($library_xml->record->tlf->asXML()) . '</div>';
    $return .= '</div>';
    $id = substr($bib, 3);
    $i++;
  }
  $return .= '</div>';
  return $return;
}
